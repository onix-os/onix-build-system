
from subprocess import PIPE, Popen
import datetime
import os

class Proc:
    output = ""
    code = ""
    error = ""
    command = ""
    def __init__(self, command=""):
        self.command = command
        with Popen(command, shell=True, stdout=PIPE, stderr=PIPE) as p:
            self.output = p.communicate()
            self.code = p.returncode
            self.error = self.output[1]
    
    def result(self):
        return self.output

    def code(self):
        return self.code
    
    def err(self):
        return self.error

    def appendLog(self, logfile=""):
        if not os.path.exists(os.path.dirname(logfile)):
            os.mkdir(os.path.dirname(logfile))  
        with open(logfile, "a") as f:
            f.write("---------------------------------------------------------------------\n")
            f.write("Command: "+ self.command+"\n")
            f.write("DateTime: "+ str(datetime.datetime.now())+"\n")
            f.write("Code: "+ (str(self.code) if self.code else "0")+"\n")
            f.write("---------------------------------------------------------------------\n")
            if self.error:
                f.write("Error: \n")
                f.write("---------------------------------------------------------------------\n")
                error = ''.join(map(chr, self.error))
                f.write((error if error else "")+"\n")
                f.write("---------------------------------------------------------------------\n")
            if self.output[0]:
                f.write("Result: \n")
                f.write("---------------------------------------------------------------------\n")
                out = ''.join(map(chr, self.output[0]))
                f.write(out+"\n")
                f.write("---------------------------------------------------------------------\n")
            f.close()
