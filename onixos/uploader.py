

import sys
import os
from os.path import exists
from .proc import Proc
import json
import configparser

class SSH_Uploader:
    repo_dir = ""
    output = ""
    uploadPath = ""
    server = ""
    user = ""
    port = ""
    command_list = {}
    keyfile = ""
    debug = []

    def __init__(self, output = "out", server = "localhost", user = "root", port = "22", uploadPath = "", subdir = "", keyfile = ""):
        directory_path = os.getcwd()
        self.uploadPath = uploadPath
        self.log_dir = directory_path+"/logs"
        if os.path.exists(self.log_dir) == False:
            os.mkdir(self.log_dir)

        self.output = output
        self.server = server
        self.user = user
        self.port = port
        self.keyfile = keyfile
        self.generateCommands()

    def generateCommands(self):
        command_list = {
            "rsync": '/usr/bin/rsync -avuP -e "/usr/bin/ssh -i {keyfile} -p {port}" {output}/* {user}@{server}:{uploadPath}',
            "scp": '/usr/bin/scp -P {port} {output}/* {user}@{server}:{uploadPath}',
        }
        command_list["rsync"] = command_list["rsync"].replace('{output}', self.output)
        command_list["rsync"] = command_list["rsync"].replace('{port}', self.port)
        command_list["rsync"] = command_list["rsync"].replace('{server}', self.server)
        command_list["rsync"] = command_list["rsync"].replace('{user}', self.user)
        command_list["rsync"] = command_list["rsync"].replace('{uploadPath}', self.uploadPath)
        command_list["rsync"] = command_list["rsync"].replace('{keyfile}', self.keyfile)

        command_list["scp"] = command_list["scp"].replace('{output}', self.repo_dir)
        command_list["scp"] = command_list["scp"].replace('{port}', self.port)
        command_list["scp"] = command_list["scp"].replace('{server}', self.server)
        command_list["scp"] = command_list["scp"].replace('{user}', self.user)
        command_list["scp"] = command_list["scp"].replace('{uploadPath}', self.uploadPath)
        command_list["scp"] = command_list["scp"].replace('{keyfile}', self.keyfile)
        self.command_list = command_list

    def upload(self):
        if exists('/usr/bin/rsync'):
            debug = Proc(self.command_list["rsync"])
            self.debug.append(debug.result())
            debug.appendLog(self.log_dir+"/rsync.log")
        elif exists('/usr/bin/scp'):
            debug = Proc(self.command_list["scp"])
            self.debug.append(debug.result())
            debug.appendLog(self.log_dir+"/rsync.log")
        else:
            print(f"No such command: %s or %s", self.command_list["rsync"], self.command_list["scp"])
