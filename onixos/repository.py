import sys
import os
from os.path import exists
from .pkgbuild import SRCINFO
from .proc import Proc
import json
import configparser

class Repository:
    repo_name = ""
    repo_url = ""
    packages = []
    package_file = ""
    command_list = {
        "makepkg": "makepkg -s -f -C --noconfirm",
        "pacinst": 'sudo pacman -S --noconfirm --needed --asdeps --overwrite "*"',
        "gitc": "git clone",
        "cp": "cp -av",
        "repoadd": "repo-add",
        "srcinfo": "makepkg --printsrcinfo > .SRCINFO",
        "tarx": "tar -zxvf",
        "curl": "curl"
    }
    current_pkg_info = {}
    output = ""
    packages_dir = ""
    repo_dir = ""
    debug = []

    repomakedepends = [
        "rsync",
        "go",
        "xmlto",
        "python-sphinx",
        "base-devel",
        "gnome-shell",
        "git",
        "qt",
        "qtcreator",
        "gperf",
        "ninja",
        "nasm",
        "base"
    ]
    repo_source = ""


    def __init__(self, repo_name = "", repo_url = "", package_file = "", output = "out", repo_source="local", source_dir="sources"):
        directory_path = os.getcwd()
        self.base_path = directory_path
        self.repo_name = repo_name
        self.repo_url = repo_url
        self.package_file = package_file
        self.output = output
        self.repo_dir = output+"/repo"
        self.packages_dir = output+"/packages"
        self.source_dir = source_dir+"/packages"
        self.repo_source = repo_source
        if os.path.exists(self.output) == False:
            os.mkdir(self.output)
        if os.path.exists(self.repo_dir) == False:
            os.mkdir(self.repo_dir)
        if os.path.exists(self.packages_dir) == False:
            os.mkdir(self.packages_dir)

        
        self.log_dir = directory_path+"/logs"
        if os.path.exists(self.log_dir) == False:
            os.mkdir(self.log_dir)
            
        self.read_packages()


    def read_packages(self):
        f = open(self.package_file,"r")
        self.packages = f.readlines()
    
    def curl_aur(self, pkg):
        if pkg != None:
            if os.path.exists(self.log_dir) == False:
                os.mkdir(self.packages_dir)
            os.chdir(self.packages_dir)
            os_command = self.command_list["curl"]+" "+self.repo_url+"/"+pkg+".tar.gz -O "+pkg+".tar.gz"
            print("[Curl] "+os_command)
            debug = Proc(os_command)
            
            debug.appendLog(self.log_dir+"/"+pkg+".log")
            if(exists(self.packages_dir+"/"+pkg+".tar.gz")):
                return

            os_command = self.command_list["tarx"]+" "+pkg+".tar.gz"
            print("[Tar] "+os_command)
            debug = Proc(os_command)
            
            debug.appendLog(self.log_dir+"/"+pkg+".log")
            os.chdir(self.base_path)
            if(exists(self.packages_dir+"/"+pkg)):
                return

    def clone(self, pkg):
        if pkg != None:
            os_command = self.command_list["gitc"]+" "+self.repo_url+"/"+pkg+".git "+self.packages_dir+"/"+pkg
            print("[Clone] "+os_command)
            if(exists(self.packages_dir+"/"+pkg)):
                return
            debug = Proc(os_command)
            
            debug.appendLog(self.log_dir+"/"+pkg+".log")

    def get_pkg_info(self, pkg):
        if(self.repo_source == "local"):
            pkgbuildPath = self.source_dir+"/"+pkg+"/.SRCINFO"
        else:
            pkgbuildPath = self.packages_dir+"/"+pkg+"/.SRCINFO"

        if(exists(pkgbuildPath) == False):
            if(self.repo_source == "local"):
                if(os.path.isdir(self.source_dir+"/"+pkg)):
                    os.chdir(self.source_dir+"/"+pkg)
                    debug = Proc(self.command_list["srcinfo"])
                    debug.appendLog(self.log_dir+"/srcinfo.log")
            else:
                if(os.path.isdir(self.packages_dir+"/"+pkg)):
                    os.chdir(self.packages_dir+"/"+pkg)
                    debug = Proc(self.command_list["srcinfo"])
                    debug.appendLog(self.log_dir+"/srcinfo.log")
            os.chdir(self.base_path)

        return [ SRCINFO(pkgbuildPath) if exists(pkgbuildPath) else False ]
        

    def build(self):
        for pkg in self.packages:
            pkg = pkg.replace("\n", "")
            pkg = pkg.replace("\t", "")
            pkg = pkg.replace(" ", "")
            if(self.repo_source == "local"):
                self.packageProc(pkg, self.source_dir)
            if(self.repo_source == "git"):
                self.clone(pkg)
                self.packageProc(pkg, self.packages_dir)
            if(self.repo_source == "aur"):
                self.curl_aur(pkg)
                self.packageProc(pkg, self.packages_dir)
            else:
                self.packageProc(pkg, self.source_dir)

    def packageProc(self, pkg, pkgDir):
        self.current_pkg_info = self.get_pkg_info(pkg)
        if(self.current_pkg_info[0] != False):
            print("[Package] Building: "+pkg)
            pkgbuildContent = dict(self.current_pkg_info[0].content)
            makedepends = pkgbuildContent.get('makedepends', "")
            depends = pkgbuildContent.get('depends', "")
            makedepends = ' '.join(makedepends) if type(makedepends) != str else makedepends
            depends = ' '.join(depends) if type(depends) != str else depends
            if(len(depends) != 0):
                print("[Depends] Required: "+depends)
                os_command = self.command_list["pacinst"]+' '+depends+' '+makedepends
                debug = Proc(os_command)
                debug.appendLog(self.log_dir+"/pacinst.log")

        if(os.path.isfile(pkgDir+"/"+pkg+"/PKGBUILD")):
            print("[Make] Package: "+self.command_list["makepkg"])
            os.chdir(pkgDir+"/"+pkg)
            debug = Proc(self.command_list["makepkg"])
            debug.appendLog(self.log_dir+"/"+pkg+".log")
            os.chdir(self.base_path)
            copyCommand = self.command_list["cp"]+" "+pkgDir+"/"+pkg+"/*.pkg.tar.* "+self.repo_dir+"/"
            print("[Copy] Package: "+pkgDir+"/"+pkg+"/*.pkg.tar.* -> "+self.repo_dir+"/")
            debug = Proc(copyCommand)
            debug.appendLog(self.log_dir+"/copy.log")

    def repomake(self):
        os.chdir(self.repo_dir)

        os_command = self.command_list["repoadd"]+' '+self.repo_name+'.db.tar.gz ./*.pkg.*'
        debug = Proc(os_command)
        debug.appendLog(self.log_dir+"/repoadd.log")
        os.chdir(self.base_path)

    def install(self):
        os_command = self.command_list["pacinst"]+' '.join(self.repomakedepends)
        debug = Proc(os_command)
        debug.appendLog(self.log_dir+"/pacinst.log")