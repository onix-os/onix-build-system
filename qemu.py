import os
import sys
import subprocess
import shutil

def check_qemu():
    # Check if qemu-system-x86_64 is available in PATH
    if shutil.which("qemu-system-x86_64") is None:
        print("Error: qemu-system-x86_64 is not installed or not in PATH.")
        sys.exit(1)

def launch_qemu(iso_path):
    # Check if the ISO path is provided and exists
    if not os.path.isfile(iso_path):
        print(f"Error: ISO file not found at {iso_path}")
        sys.exit(1)

    # Set virtual machine parameters
    ram = "4G"            # Amount of RAM
    cpu = "x86_64"        # CPU architecture
    cpu_cores = 1         # Number of CPU cores

    # QEMU command to launch the VM
    command = [
        f"qemu-system-{cpu}",
        "-m", ram,
        "-smp", f"cores={cpu_cores}",
        "-cdrom", iso_path,
        "-boot", "d",
        #"-vga", "std",
        "-name", "ISO_Test_VM",
        "-net", "nic", "-net", "user",
        #"-rtc", "base=localtime"
        "-vga", "qxl"
    ]

    # Run the QEMU command
    try:
        subprocess.run(command, check=True)
    except subprocess.CalledProcessError as e:
        print(f"Failed to launch QEMU: {e}")
        sys.exit(1)

if __name__ == "__main__":
    check_qemu()

    if len(sys.argv) != 2:
        print("Usage: python launch_qemu.py <path_to_iso>")
        sys.exit(1)

    iso_path = sys.argv[1]
    launch_qemu(iso_path)
