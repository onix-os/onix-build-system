#!/bin/env bash

export SERVER='192.168.1.22'
export USER='onix-project'
export REPO_PATH='/home/onix-project/domains/repo.onix-project.com/public_html/'
export ISO_PATH='/home/onix-project/domains/pub.onix-project.com/public_html/' 

sudo python clean.py
python build.py repos
#rsync -av output/aur/repo/* $USER@$SERVER:$REPO_PATH/aur/
#rsync -av output/base/repo/* $USER@$SERVER:$REPO_PATH/base/

repos="aur base"
for repo in $repos; do
    if [ -d output/$repo/repo/ ]; then
        rsync -av output/$repo/repo/* $USER@$SERVER:$REPO_PATH/$repo/
    fi
done

sudo python clean.py
sudo python build.py iso
#python build.py iso-sync

profiles="core xfce gnome icewm"
for profile in $profiles; do
    if [ -d output/iso/$profile/output/ ]; then
        rsync -av output/iso/$profile/output/* $USER@$SERVER:$ISO_PATH
    fi
done