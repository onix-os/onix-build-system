#!/bin/bash

# Check if an ISO file path is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <path_to_iso>"
    exit 1
fi

ISO_PATH="$1"

# Check if the specified ISO file exists
if [ ! -f "$ISO_PATH" ]; then
    echo "Error: ISO file not found at $ISO_PATH"
    exit 1
fi

# Set virtual machine parameters
RAM="2G"                  # Amount of RAM
CPU="x86_64"              # CPU architecture
CPU_CORES=1               # Number of CPU cores

# Launch the QEMU virtual machine
qemu-system-$CPU \
    -m $RAM \
    -smp cores=$CPU_CORES \
    -cdrom "$ISO_PATH" \
    -boot d \
    -vga std \
    -enable-kvm \
    -name "ISO_Test_VM" \
    -net nic -net user \
    -rtc base=localtime
