<?php
include_once 'PHPObfuscator.php';

$pharPath = Phar::running();
$pharDir = dirname($pharPath);
$currentDir = str_replace("phar://", "", $pharDir);

if (count($argv) < 3) {
    echo "Usage: autoload.php <key> <path>\n";
    exit(1);
}

// Get the encryption key and path from command-line arguments
$key = $argv[1];
$path = $argv[2];

$obfuscator = new PHPObfuscator($currentDir."/".$key);

// Autoloader function
spl_autoload_register(function ($class) use ($obfuscator, $currentDir) {
    // Decrypt and include the encrypted class file
    $filePath =  $currentDir . '/'. $path . '/' . str_replace('\\', '/', $class) . '.php';
    try{
        if (!file_exists($filePath)) {
            throw new Exception("File does not exist: $filePath");
        }
        $encryptedCode = file_get_contents($filePath);
        $decryptedCode = $obfuscator->decrypt($encryptedCode);
        if ($obfuscator->isSafeToEval($decryptedCode)) {
            eval('?>' . $decryptedCode);
        } else {
            throw new Exception("Unsafe Code!\n");
        }
    }
    catch(Exception  $e){
        echo "An error occurred in file: $filePath - " . $e->getMessage();
        exit();
    }
});
