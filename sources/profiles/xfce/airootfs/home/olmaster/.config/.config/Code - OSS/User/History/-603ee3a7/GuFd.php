<?php

class PHPObfuscator {
    private $key;

    public function __construct($keyFile) {
        $this->key = file_get_contents($keyFile);
        if ($this->key === false) {
            throw new Exception("Failed to read encryption key from file: $keyFile");
        }
    }

    public function encrypt($data) {
        return base64_encode($this->xorEncrypt($data));
    }

    public function decrypt($data) {
        return $this->xorEncrypt(base64_decode($data));
    }

    private function xorEncrypt($data) {
        $key = $this->key;
        $keyLen = strlen($key);
        $dataLen = strlen($data);
        $encrypted = '';

        for ($i = 0; $i < $dataLen; ++$i) {
            $encrypted .= $data[$i] ^ $key[$i % $keyLen];
        }

        return $encrypted;
    }

    // Function to recursively encrypt files in a directory
    public function encryptDirectory($sourceDir, $destDir) {
        $files = scandir($sourceDir);
        foreach ($files as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }

            $sourcePath = $sourceDir . DIRECTORY_SEPARATOR . $file;
            $destPath = $destDir . DIRECTORY_SEPARATOR . $file;

            if (is_dir($sourcePath)) {
                // If it's a directory, create the corresponding directory in the destination
                if (!file_exists($destPath)) {
                    mkdir($destPath, 0777, true);
                }
                // Recursively encrypt files in the subdirectory
                $this->encryptDirectory($sourcePath, $destPath);
            } elseif (is_file($sourcePath) && pathinfo($sourcePath, PATHINFO_EXTENSION) === 'php') {
                // Encrypt PHP files and save to the destination directory with the same directory structure
                $fileContents = file_get_contents($sourcePath);
                $encryptedContents = $this->encrypt($fileContents);
                file_put_contents($destPath, $encryptedContents);
            }
        }
    }

    function isSafeToEval($code) {
        // Check code length
        if (strlen($code) < 1) {
            return false;
        }
        
        // Check for dangerous functions
        if (preg_match('/\b(exec|system|shell_exec|passthru|popen|proc_open)\b/i', $code)) {
            return false;
        }

        $tempFile = tempnam(sys_get_temp_dir(), 'php_code_');
        file_put_contents($tempFile, $code);
        $returnVar = 0;
        @exec("php -l $tempFile", $output, $returnVar);

        if ($returnVar !== 0) {
            return false;
        }
    
        // Additional checks as needed
    
        return true;
    }

    // Function to recursively encrypt files in a directory
    public function decryptDirectory($sourceDir) {
        $files = scandir($sourceDir);
        foreach ($files as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }

            $sourcePath = $sourceDir . DIRECTORY_SEPARATOR . $file;

            if (is_dir($sourcePath)) {
                $this->decryptDirectory($sourcePath);
            } elseif (is_file($sourcePath) && pathinfo($sourcePath, PATHINFO_EXTENSION) === 'php') {
                try{
                    if (!file_exists($sourcePath)) {
                        throw new Exception("File does not exist: $sourcePath");
                    }
                    // Encrypt PHP files and save to the destination directory with the same directory structure
                    $fileContents = file_get_contents($sourcePath);
                    $encryptedContents = $this->decrypt($fileContents);
                    if ($this->isSafeToEval($encryptedContents)) {
                        eval('?>' . $encryptedContents);
                    } else {
                        throw new Exception("Unsafe Code!\n");
                    }
                }catch(Exception  $e){
                    echo "An error occurred in file: $sourcePath - " . $e->getMessage();
                }
            }
        }
    }
}