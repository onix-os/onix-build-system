<?php
include_once 'PHPObfuscator.php';

$obfuscator = new PHPObfuscator('.key');

// Decrypt and execute the autoloader
$decryptedCode = $obfuscator->decrypt(file_get_contents(__FILE__));
eval($decryptedCode);

// Autoloader function
spl_autoload_register(function ($class) use ($obfuscator) {
    // Decrypt and include the encrypted class file
    $filePath = __DIR__ . '/' . str_replace('\\', '/', $class) . '.enc';
    $encryptedCode = file_get_contents($filePath);
    $decryptedCode = $obfuscator->decrypt($encryptedCode);
    eval($decryptedCode);
});
