#!/usr/bin/env python
from onixos.repository import Repository
from onixos.iso import ISO
from onixos.uploader import SSH_Uploader
import os
import sys
import configparser

def repos():
    """
    This function is used to synchronize the AUR and local package repositories.

    Args:
        None

    Returns:
        None

    """
    aurRepo = Repository("onix-aur",
                    "https://aur.archlinux.org/cgit/aur.git/snapshot",
                    "sources/aur.list",
                    "output/aur",
                    "aur",
                    "sources")
    aurRepo.build()
    aurRepo.repomake()

    baseRepo = Repository("onix-base",
                        "",
                        "sources/packages.list",
                        "output/base",
                        "local",
                        "sources")
    baseRepo.install()
    baseRepo.build()
    baseRepo.repomake()

def repoSync():
    """
    This function is used to synchronize the AUR and local package repositories.

    Args:
        None

    Returns:
        None

    """
    
    config_files = ['sources/sync/sourceforge.conf', 'sources/sync/local.conf']
    repos = ['aur', 'base']
    
    for config_file in config_files:
        if os.path.isfile(config_file):
            config = configparser.ConfigParser()
            config.read(config_file)
            syncConfig = config['sync']
            
            for repo in repos:
                uploader = SSH_Uploader(
                    f"output/{repo}/repo",
                    syncConfig['server'],
                    syncConfig['user'],
                    syncConfig['port'],
                    syncConfig['path'] + f'/{repo}',
                    "",
                    syncConfig['keyfile']
                )
                uploader.upload()

def iso():
    """
    This function is used to create ISO images from the specified profiles.

    Args:
        profiles_file (str): The path to the profiles file.

    Returns:
        None

    """
    isos = ISO('sources/profiles.list')
    isos.install()
    isos.build()

def isoSync():
    """
    This function is used to create ISO images from the specified profiles.

    Args:
        profiles_file (str): The path to the profiles file.

    Returns:
        None

    """
    isos = ISO('sources/profiles.list')
    isos.sync()

def main():
    """
    This function is the main entry point of the script.

    Args:
        None

    Returns:
        None

    """
    args = sys.argv
    if len(args) > 1:
        param = args[1]
        if (param == "iso"):
            iso()
        elif (param == "iso-sync"):
            isoSync()
        elif (param == "repos"):
            repos()
        elif (param == "upload"):
            repoSync()
        else:
            print("Command not found. Try: repos, iso, i upload.")
    else:
        repos()
        iso()


if __name__ == '__main__':
    main()
