<?php
include_once 'PHPObfuscator.php';

$obfuscator = new PHPObfuscator('.key');

// Autoloader function
spl_autoload_register(function ($class) use ($obfuscator) {
    // Decrypt and include the encrypted class file
    $filePath = __DIR__ . '/../../' . '/' . str_replace('\\', '/', $class) . '.php';
    try{
        if (!file_exists($filePath)) {
            throw new Exception("File does not exist: $filePath");
        }
        $encryptedCode = file_get_contents($filePath);
        $decryptedCode = $obfuscator->decrypt($encryptedCode);
        if ($obfuscator->isSafeToEval($encryptedContents)) {
            eval('?>' . $encryptedContents);
        } else {
            throw new Exception("Unsafe Code!\n");
        }
    }
    catch(Exception  $e){
        echo "An error occurred in file: $filePath - " . $e->getMessage();
        exit();
    }
});
