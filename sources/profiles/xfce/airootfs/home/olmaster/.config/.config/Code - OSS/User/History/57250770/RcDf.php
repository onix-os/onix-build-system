<?php
include_once 'PHPObfuscator.php';

$pharPath = Phar::running();
$pharDir = dirname($pharPath);
$currentDir = str_replace("phar://", "", $pharDir);

$key = '.key';
$path = 'encrypted';

if(is_file($currentDir.'/config.php')){
    $config = include $currentDir.'/config.php';
    if(isset($config['key']) && isset($config['path'])){
        $key = $config['key'];
        $path = $config['path'];
    }
}

// Get the encryption key and path from command-line arguments


$obfuscator = new PHPObfuscator($currentDir."/".$key);

// Autoloader function
spl_autoload_register(function ($class) use ($obfuscator, $currentDir, $path) {
    // Decrypt and include the encrypted class file
    $filePath =  $currentDir . '/'. $path . '/' . str_replace('\\', '/', $class) . '.php';
    try{
        if (!file_exists($filePath)) {
            throw new Exception("File does not exist: $filePath");
        }
        $encryptedCode = file_get_contents($filePath);
        $decryptedCode = $obfuscator->decrypt($encryptedCode);
        if ($obfuscator->isSafeToEval($decryptedCode)) {
            eval('?>' . $decryptedCode);
        } else {
            throw new Exception("Unsafe Code!\n");
        }
    }
    catch(Exception  $e){
        echo "An error occurred in file: $filePath - " . $e->getMessage();
        exit();
    }
});
