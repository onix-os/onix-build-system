# PHPObfuscator

PHPObfuscator is a tool for encrypting and obfuscating PHP code, providing an additional layer of security for your sensitive codebase.

## Features

- Encrypts PHP files to protect sensitive code
- Obfuscates code to make it harder to reverse-engineer
- Supports autoloading encrypted files

## Installation

1. Clone the repository or download the latest release from [Gitlab](https://gitlab.com/oytunistrator/php-obfuscator).

## Usage

### Encryption

To encrypt your PHP files, use the `encryptDirectory` method provided by the `PHPObfuscator` class. This method recursively encrypts all PHP files in a directory.

```php
$obfuscator = new PHPObfuscator('your_secret_key');
$obfuscator->encryptDirectory('/path/to/source', '/path/to/destination');
```

### Autoloading

PHPObfuscator also provides an autoloading mechanism for encrypted files. You can include the `autoload.php` file in your project to automatically load encrypted files.

```php
require_once 'path/to/PHPObfuscator/autoload.php';
```

### Decryption

Decryption of encrypted files can be done using the `decryptDirectory` method. This method decrypts all encrypted PHP files in a directory.

```php
$obfuscator = new PHPObfuscator('your_secret_key');
$obfuscator->decryptDirectory('/path/to/encrypted');
```

## Security Considerations

- Keep your encryption key (`your_secret_key`) secure and do not expose it in your codebase.
- Encrypt only the sensitive parts of your codebase to minimize performance overhead.

## License

PHPObfuscator is licensed under the MIT License. See [LICENSE](LICENSE) for more information.

---

Feel free to customize this template with more detailed instructions, examples, or additional sections based on your specific needs and features of PHPObfuscator.