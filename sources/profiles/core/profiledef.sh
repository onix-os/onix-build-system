#!/usr/bin/env bash
# shellcheck disable=SC2034

VERSION="$(date +%y%m%d%H%M)"
iso_name="onixos-core"
iso_label="ONIX"
iso_publisher="Oytun Ozdemir <oytunozdemir@yandex.com>"
iso_application="OnixOS Core Edition"
iso_version="$VERSION"
install_dir="onixos"
#bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito' 'uefi-x64.systemd-boot.esp' 'uefi-x64.systemd-boot.eltorito')
bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito')
arch="x86_64"
pacman_conf="pacman.conf"
