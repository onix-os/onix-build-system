#!/bin/bash

set -e -u

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/
chmod 700 /root

sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf

systemctl set-default graphical.target
systemctl enable acpid
systemctl enable ntpd
systemctl enable cups
systemctl enable slim
systemctl enable NetworkManager
locale-gen

groupadd olmaster

useradd --create-home -m -g olmaster -G root -s /bin/fish olmaster 
usermod --password $(openssl passwd olmaster) olmaster
usermod --password $(openssl passwd toor) root
usermod -s /usr/bin/fish olmaster
usermod -s /usr/bin/fish root

chown -Rv olmaster:olmaster /home/olmaster/
chmod 775 -Rv /home/olmaster/

export HPATH=$(runuser -l olmaster -c "echo $PATH")

echo "set -gx GOPATH $HOME/Go" >> /home/olmaster/.config/fish/config.fish
echo "set -gx PATH $HPATH $HOME/Go/bin" >> /home/olmaster/.config/fish/config.fish

echo "olmaster ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

echo "exec icewm" >> /home/olmaster/.xinitrc

echo "[Icon Theme]" > /usr/share/icons/default/index.theme
echo "Inherits=core" >> /usr/share/icons/default/index.theme


#echo "[daemon]" >> /etc/gdm/custom.conf 
#echo "AutomaticLoginEnable=True" >> /etc/gdm/custom.conf 
#echo "AutomaticLogin=olmaster" >> /etc/gdm/custom.conf 

runuser -l olmaster -c "LC_ALL=C xdg-user-dirs-update --force"
