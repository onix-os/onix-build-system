<?php

class PHPObfuscator {
    private $key;

    public function __construct($keyFile) {
        $this->key = file_get_contents($keyFile);
        if ($this->key === false) {
            throw new Exception("Failed to read encryption key from file: $keyFile");
        }
    }

    public function encrypt($data) {
        return base64_encode($this->xorEncrypt($data));
    }

    public function decrypt($data) {
        return $this->xorEncrypt(base64_decode($data));
    }

    private function xorEncrypt($data) {
        $key = $this->key;
        $keyLen = strlen($key);
        $dataLen = strlen($data);
        $encrypted = '';

        for ($i = 0; $i < $dataLen; ++$i) {
            $encrypted .= $data[$i] ^ $key[$i % $keyLen];
        }

        return $encrypted;
    }

    // Function to recursively encrypt files in a directory
    public function encryptDirectory($sourceDir, $destDir) {
        $files = scandir($sourceDir);
        foreach ($files as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }

            $sourcePath = $sourceDir . DIRECTORY_SEPARATOR . $file;
            $destPath = $destDir . DIRECTORY_SEPARATOR . $file;

            if (is_dir($sourcePath)) {
                // If it's a directory, create the corresponding directory in the destination
                if (!file_exists($destPath)) {
                    mkdir($destPath, 0777, true);
                }
                // Recursively encrypt files in the subdirectory
                $this->encryptDirectory($sourcePath, $destPath);
            } elseif (is_file($sourcePath) && pathinfo($sourcePath, PATHINFO_EXTENSION) === 'php') {
                // Encrypt PHP files and save to the destination directory with the same directory structure
                $fileContents = file_get_contents($sourcePath);
                $encryptedContents = $this->encrypt($fileContents);
                file_put_contents($destPath, $encryptedContents);
            }
        }
    }

    // Function to recursively encrypt files in a directory
    public function decryptDirectory($sourceDir) {
        $files = scandir($sourceDir);
        foreach ($files as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }

            $sourcePath = $sourceDir . DIRECTORY_SEPARATOR . $file;
            $destPath = $destDir . DIRECTORY_SEPARATOR . $file;

            if (is_dir($sourcePath)) {
                $this->decryptDirectory($sourcePath, $destPath);
            } elseif (is_file($sourcePath) && pathinfo($sourcePath, PATHINFO_EXTENSION) === 'php') {
                // Encrypt PHP files and save to the destination directory with the same directory structure
                $fileContents = file_get_contents($sourcePath);
                $encryptedContents = $this->decrypt($fileContents);
                eval($decryptedCode);
            }
        }
    }
}