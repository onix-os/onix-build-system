import os
from .proc import Proc
from onixos.uploader import SSH_Uploader
import configparser

class ISO:
    profile = ""
    debug = []
    syncConfig = {}

    isomakedepends = [
        "rsync",
        "go",
        "xmlto",
        "python-sphinx",
        "base-devel",
        "gnome-shell",
        "git",
        "qt",
        "qtcreator",
        "gperf",
        "ninja",
        "nasm",
        "git-lfs ",
        "base-devel",
        "arch-install-scripts",
        "squashfs-tools",
        "dosfstools",
        "syslinux ",
        "git ",
        "imagemagick",
        "archiso",
        "sudo",
    ]

    command_list = {
        "pacinst": 'sudo pacman -S --noconfirm --needed --asdeps --overwrite "*"',
        "build": 'sudo mkarchiso -C "{sourcedir}/pacman.conf" -v -w "{workdir}/" -o "{outdir}" -L "ONIXOS" "$@" "{sourcedir}"',
        "clean": 'sudo rm -rf {workdir}',
    }

    source_dir = ""
    profile_dir = ""
    build_dir = ""
    work_dir = ""
    build_command = ""
    profiles = []
    profile_file = ""

    def __init__(self, profile_file = "sources/profiles.list"):
        self.profile_file = profile_file
        directory_path = os.getcwd()
        self.log_dir = directory_path+"/logs"
        if os.path.exists(self.log_dir) == False:
            os.mkdir(self.log_dir)



    def mkarchiso(self, profile="", source_dir="sources", output="output"):
        directory_path = os.getcwd()
        self.profile = profile
        self.source_dir = directory_path+"/"+source_dir+"/profiles/"+profile
        self.work_dir = directory_path+"/"+output+"/iso/"+profile+"/work"
        self.build_dir = directory_path+"/"+output+"/iso/"+profile+"/output"

        if os.path.exists(output) == False:
            os.mkdir(output)
        if os.path.exists(output+"/iso/") == False:
            os.mkdir(output+"/iso/")
        if os.path.exists(output+"/iso/"+profile) == False:
            os.mkdir(output+"/iso/"+profile)

        self.build_command = self.command_list["build"]
        self.build_command = self.build_command.replace("{sourcedir}", self.source_dir)
        self.build_command = self.build_command.replace("{workdir}", self.work_dir)
        self.build_command = self.build_command.replace("{outdir}", self.build_dir)

        self.clean_command = self.command_list["clean"]
        self.clean_command = self.clean_command.replace("{workdir}", self.work_dir)


        os.chdir(self.source_dir)
        print("[Clean] "+self.clean_command)
        debug = Proc(self.clean_command)
        self.debug.append(debug)
        debug.appendLog(self.log_dir+"/isobuild.log")

        print("[Build] "+self.build_command)
        debug = Proc(self.build_command)
        self.debug.append(debug)
        debug.appendLog(self.log_dir+"/isobuild.log")

        os.chdir(directory_path)



    def build(self):
        f = open(self.profile_file,"r")
        self.profiles = f.readlines()
        for prof in self.profiles:
            prof = prof.replace("\n", "")
            prof = prof.replace("\t", "")
            prof = prof.replace(" ", "")
            self.mkarchiso(prof)
            self.upload(prof)

    def install(self):
        os_command = self.command_list["pacinst"]+' '.join(self.isomakedepends)
        debug = Proc(os_command)
        self.debug.append(debug)
        debug.appendLog(self.log_dir+"/pacinst.log")

    def upload(self, profile):
        config_files = ['sources/sync/sourceforge.conf', 'sources/sync/local.conf']

        for config_file in config_files:
            if os.path.isfile(config_file):
                config = configparser.ConfigParser()
                config.read(config_file)
                self.syncConfig = config['sync']

                iso = SSH_Uploader(
                    f"output/iso/{profile}/output",
                    self.syncConfig['server'],
                    self.syncConfig['user'],
                    self.syncConfig['port'],
                    self.syncConfig['path'] + '/ISO',
                    "",
                    self.syncConfig['keyfile']
                )
                iso.upload()
