<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
include '../PHPObfuscator.phar';

$obfuscator = new PHPObfuscator('.key');
$obfuscator->decryptDirectory(__DIR__.'/encrypted');
