<?php
ini_set('phar.readonly', 0);

// Create a new Phar archive
$phar = new Phar('PHPObfuscator.phar');

// Add the PHPObfuscator.php file to the archive
$phar->buildFromDirectory(__DIR__.'/src', '/\.php$/');

// Set the main file that will be executed when the PHAR is run
$phar->setDefaultStub('PHPObfuscator.php');

// Compress the PHAR archive using Gzip
$phar->compressFiles(Phar::GZ);

echo "PHAR file created successfully: PHPObfuscator.phar\n";



// Create a new Phar archive
$phar = new Phar('PHPObfuscatorAutoload.phar');

// Add the PHPObfuscator.php file to the archive
$phar->buildFromDirectory(__DIR__.'/src', '/\.php$/');

// Set the main file that will be executed when the PHAR is run
$phar->setDefaultStub('autoload.php');
$phar->setStub('<?php include "phar://PHPObfuscatorAutoload.phar/autoload.php"; __HALT_COMPILER();');

// Compress the PHAR archive using Gzip
$phar->compressFiles(Phar::GZ);

echo "PHAR file created successfully: PHPObfuscatorAutoload.phar\n";