<?php
include_once 'PHPObfuscator.php';

if ($argc < 3) {
    echo "Usage: php script.php <folder> <key>\n";
    exit(1);
}


$keyFile = $argv[1];
$folder = $argv[2];

$obfuscator = new PHPObfuscator($keyFile);

// Autoloader function
spl_autoload_register(function ($class) use ($obfuscator) {
    // Decrypt and include the encrypted class file
    $filePath = $folder . '/' . str_replace('\\', '/', $class) . '.php';
    $encryptedCode = file_get_contents($filePath);
    $decryptedCode = $obfuscator->decrypt($encryptedCode);
    eval($decryptedCode);
});
