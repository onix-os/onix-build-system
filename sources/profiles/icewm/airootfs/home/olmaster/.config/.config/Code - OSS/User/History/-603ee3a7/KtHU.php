<?php

class PHPObfuscator {
    private $key;

    public function __construct($keyFile) {
        $this->key = file_get_contents($keyFile);
        if ($this->key === false) {
            throw new Exception("Failed to read encryption key from file: $keyFile");
        }
        $this->key = $key;
    }

    public function encrypt($data) {
        return base64_encode($this->xorEncrypt($data));
    }

    public function decrypt($data) {
        return $this->xorEncrypt(base64_decode($data));
    }

    private function xorEncrypt($data) {
        $key = $this->key;
        $keyLen = strlen($key);
        $dataLen = strlen($data);
        $encrypted = '';

        for ($i = 0; $i < $dataLen; ++$i) {
            $encrypted .= $data[$i] ^ $key[$i % $keyLen];
        }

        return $encrypted;
    }

    // Function to recursively encrypt files in a directory
    public function encryptDirectory($directory) {
        $files = scandir($directory);
        foreach ($files as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }

            $filePath = $directory . DIRECTORY_SEPARATOR . $file;
            if (is_dir($filePath)) {
                $this->encryptDirectory($filePath); // Recursive call for subdirectories
            } elseif (is_file($filePath) && pathinfo($filePath, PATHINFO_EXTENSION) === 'php') {
                // Encrypt PHP files
                $fileContents = file_get_contents($filePath);
                $encryptedContents = $this->encrypt($fileContents);
                file_put_contents($filePath, $encryptedContents);
            }
        }
    }
}