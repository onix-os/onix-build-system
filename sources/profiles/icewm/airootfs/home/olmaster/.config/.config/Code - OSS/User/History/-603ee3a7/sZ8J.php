<?php

class PHPObfuscator {
    private $key;

    public function __construct($keyFile) {
        $this->key = file_get_contents($keyFile);
        if ($this->key === false) {
            throw new Exception("Failed to read encryption key from file: $keyFile");
        }
        $this->key = $key;
    }

    public function encrypt($data) {
        return base64_encode($this->xorEncrypt($data));
    }

    public function decrypt($data) {
        return $this->xorEncrypt(base64_decode($data));
    }

    private function xorEncrypt($data) {
        $key = $this->key;
        $keyLen = strlen($key);
        $dataLen = strlen($data);
        $encrypted = '';

        for ($i = 0; $i < $dataLen; ++$i) {
            $encrypted .= $data[$i] ^ $key[$i % $keyLen];
        }

        return $encrypted;
    }
}