FROM olproject/onixos:latest

RUN updater
RUN pacman -Sy --noconfirm yay-git rsync go xmlto python-sphinx base-devel go gnome-shell git qtcreator gperf ninja nasm openssh openssl sudo python nodejs node-gyp archiso python --overwrite "*"
RUN echo "buildkit ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
RUN groupadd buildkit
RUN useradd -ms /bin/bash buildkit -g buildkit
RUN chown -Rv buildkit:buildkit /home/buildkit
USER buildkit
RUN git clone https://gitlab.com/onix-os/onix-build-system /home/buildkit/builder
ENV PATH=$PATH:/usr/bin:/bin:/home/buildkit/builder
WORKDIR /home/buildkit/builder
CMD ["python", "build.py"]
VOLUME ["/home/buildkit/builder/output"]
