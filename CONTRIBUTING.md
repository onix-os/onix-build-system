# Contributing to Onix Build Scripts

Thank you for considering contributing to the Onix Build Scripts project! We appreciate your help in improving this project and welcome contributions from everyone. To ensure a smooth collaboration, please take a moment to read through this guide.

## How to Contribute

### 1. Reporting Bugs
If you encounter a bug while using Onix Build Scripts, please help us by reporting it.

#### Steps to Report a Bug
1. **Search for Existing Issues**: Before submitting a new issue, please check the [issue tracker](https://gitlab.com/onix-os/onix-build-system/-/issues) to see if it has already been reported.
2. **Create a New Issue**: If no similar issue exists, create a new issue and include:
   - A clear and descriptive title.
   - Steps to reproduce the bug.
   - Expected and actual behavior.
   - Relevant logs, error messages, or screenshots.
   - Your environment details (OS, shell, Onix version).

### 2. Suggesting Enhancements
Enhancements are welcome! If you have an idea to improve the project:

1. **Check for Similar Suggestions**: Review the [issue tracker](#) for existing enhancement requests.
2. **Open a New Issue**: If your idea is unique, create an issue titled "Enhancement: [Your Idea]" and describe:
   - The motivation and use case.
   - Proposed implementation details.
   - Any alternatives considered.

### 3. Submitting Code Contributions
We welcome contributions for bug fixes, features, and documentation updates.

#### Code Contribution Guidelines
1. **Fork the Repository**: Fork the [Onix Build Scripts repository](https://gitlab.com/onix-os/onix-build-system/).
2. **Create a Branch**: Create a feature or bugfix branch (`feature/your-feature-name` or `bugfix/issue-number`).
3. **Write Clear Commit Messages**: Each commit message should be concise and descriptive.
4. **Follow Coding Standards**: Ensure your code adheres to project coding conventions.
5. **Add or Update Tests**: If applicable, write tests for your changes.
6. **Document Your Changes**: Update any relevant documentation.
7. **Open a Pull Request (PR)**:
   - Ensure your PR title and description are clear and informative.
   - Link the related issue, if applicable.
   - Explain the purpose and scope of your changes.

### 4. Code Style and Standards
- Follow the existing coding style as closely as possible.
- Use meaningful variable and function names.
- Ensure scripts are POSIX-compliant unless otherwise specified.
- Test your scripts thoroughly.

### 5. Review Process
All pull requests will be reviewed by maintainers. Be prepared to make revisions if requested.

### 6. Licensing
By contributing to Onix Build Scripts, you agree that your contributions will be licensed under the same license as the project.

## Communication
For questions or discussions:
- Use the [issue tracker](https://gitlab.com/onix-os/onix-build-system/-/issues).
- Follow the project's [Code of Conduct](https://gitlab.com/onix-os/onix-build-system/).

Thank you for helping improve Onix Build Scripts! We look forward to your contributions.
