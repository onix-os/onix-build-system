#!/usr/bin/env python
import os
from onixos.proc import Proc

command = {
    "clean": "sudo rm -rfv output/*",
}

debug = []

if __name__ == '__main__':
    """
    This function is the main entry point of the script.

    Args:
        None

    Returns:
        None

    """
    directory_path = os.getcwd()
    log_dir = directory_path+"/logs"
    if os.path.exists(log_dir) == False:
        os.mkdir(log_dir)
    debugProc = Proc("sudo rm -rf "+log_dir+"/*.log")
    debug.append(debugProc.result())
    debugProc = Proc(command["clean"])
    debug.append(debugProc.result())
    debugProc.appendLog(log_dir+"/clean.log")