#!/usr/bin/env python
import os
import sys

command = {
    "build": "docker build . -t olproject/buildkit --no-cache",
    "publish": "docker push olproject/buildkit",
    "shell": "docker run -it olproject/buildkit bash",
    "test-build": "docker run -it olproject/buildkit build.py",
}

debug = []

if __name__ == '__main__':
    """
    This function is the main entry point of the script.

    Args:
        None

    Returns:
        None

    """
    args = sys.argv
    if len(args) > 1:
        param = args[1]
        if (param == "build"):
            debug.append(os.system(command["build"]))
            debug.append(os.system(command["publish"]))
        elif (param == "test"):
            debug.append(os.system(command["build"]))
            debug.append(os.system(command["test-build"]))
        elif (param == "shell"):
            debug.append(os.system(command["shell"]))
        else:
            print("Command not found. Try: build, shell.")
    else:
        debug.append(os.system(command["build"]))
        debug.append(os.system(command["publish"]))
