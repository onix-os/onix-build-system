all: clean iso

clean:
	sudo python clean.py

iso:
	sudo python build.py iso

repos:
	python build.py repos